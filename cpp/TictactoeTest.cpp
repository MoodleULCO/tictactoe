#include "Tictactoe.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

void testerJeu(const std::string & str, const Jeu & jeu) {
    std::stringstream oss;
    oss << jeu;
    CHECK_EQUAL(str, oss.str());
}

TEST_GROUP(GroupTictactoe) { };

TEST(GroupTictactoe, test_Jeu) { 
    Jeu jeu;
    // TODO
    CHECK(true);
}

TEST(GroupTictactoe, test_Jeu1) { 
    Jeu jeu;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            CHECK_EQUAL(jeu.grille[i][j], ".");
        }
    } 
}

TEST(GroupTictactoe, test_Jeu2) { 
    Jeu jeu;
    CHECK_EQUAL(jeu.jouer(1,1), true);
}

TEST(GroupTictactoe, test_Jeu3) { 
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(0,1);
    jeu.jouer(1,1);
    jeu.jouer(0,2);
    jeu.jouer(2,2);

    CHECK_EQUAL(jeu.getVainqueur(), JOUEUR_ROUGE);
}

TEST(GroupTictactoe, test_Jeu4) { 
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(0,1);
    jeu.jouer(1,1);
    jeu.jouer(0,2);
    jeu.jouer(2,2);

    CHECK_EQUAL(jeu.jouer(2,0), false);
}

TEST(GroupTictactoe, test_Jeu5) { 
    Jeu jeu;
    jeu.jouer(1,1);
    CHECK_EQUAL(jeu.jouer(1,1), false);
}

TEST(GroupTictactoe, test_Jeu6) { 
    Jeu jeu;
    jeu.jouer(1,1);
    CHECK_EQUAL(jeu.jouer(1,1), false);
}

TEST(GroupTictactoe, test_Jeu7) { 
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(0,1);
    jeu.jouer(0,2);

    jeu.jouer(2,0);     //OXO
    jeu.jouer(2,1);     //OXO
    jeu.jouer(2,2);     //XOX

    jeu.jouer(1,0);
    jeu.jouer(1,1);
    jeu.jouer(1,2);

    CHECK_EQUAL(jeu.getVainqueur(), JOUEUR_EGALITE);
}
