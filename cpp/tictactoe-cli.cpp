#include "Tictactoe.hpp"

using namespace std;

int main() {

	Jeu jeu;
	int i,j;
	jeu.afficherGrille();

	while(true){
		if(jeu.getVainqueur() == JOUEUR_VIDE){
			std::cout << "Quel coup voulez-vous jouer (2 valeurs entre 0 et 2) -> " << jeu.getJoueurCourant() << " : ";
			std::cin >> i >> j;
			
			if(!jeu.jouer(i,j)){
				std::cout << "/!\\ Case occupee /!\\ " << std::endl;
			}
			jeu.afficherGrille();
		}else{
			if(jeu.getVainqueur() == JOUEUR_VERT){
				std::cout << "Vainqueur Joueur Vert" << std::endl;
			}
			if(jeu.getVainqueur() == JOUEUR_ROUGE){
				std::cout << "Vainqueur Joueur Rouge" << std::endl;
			}
			if(jeu.getVainqueur() == JOUEUR_EGALITE){
				std::cout << "Egalite" << std::endl;
			}
			break;
		}
	}
	return 0;
}

