#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
	tour = 0;

	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			grille[i][j] = ".";
		}
	}
}

void Jeu::afficherGrille(){
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			std::cout << grille[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    // TODO
    return os;
}

Joueur Jeu::getVainqueur() const {
	// Diagnonale 1
    if( (grille[0][0] == grille[1][1] && grille[0][0] == grille[2][2] && grille[0][0] != "." ) ){
		if(grille[0][0] == "O"){
			return JOUEUR_ROUGE;
		}else{
			return JOUEUR_VERT;
		}
	}
	// Diagonale 2
	else if( (grille[0][2] == grille[1][1] && grille[0][2] == grille[2][0] && grille[0][2] != "." ) ){
		if(grille[0][2] == "O"){
			return JOUEUR_ROUGE;
		}else{
			return JOUEUR_VERT;
		}
	}else{
		for(int i = 0; i < 3; i++){
			// Ligne
			if( (grille[i][0] == grille[i][1] && grille[i][0] == grille[i][2] && grille[i][0] != "." ) ){
				if(grille[i][0] == "O"){
					return JOUEUR_ROUGE;
				}else{
					return JOUEUR_VERT;
				}
			}
			// Colonne
			else if( (grille[0][i] == grille[1][i] && grille[0][i] == grille[2][i] && grille[0][i] != "." ) ){
				if(grille[0][i] == "O"){
					return JOUEUR_ROUGE;
				}else{
					return JOUEUR_VERT;
				}
			}
			// Egalite
			else if( (grille[0][i] != "." && grille[1][i] != "." && grille[2][i] != ".") ){
				return JOUEUR_EGALITE;
			}
			// Pas de vainqueur, ni d'egalite
			else{
				return JOUEUR_VIDE;
			}
		}
	}
}

Joueur Jeu::getJoueurCourant() const {
    if(tour%2 == 0){
		return JOUEUR_ROUGE;
    }else{
		return JOUEUR_VERT;
    }
}

bool Jeu::jouer(int i, int j) {
	if(getVainqueur() == JOUEUR_VIDE){
		if( (getJoueurCourant() == JOUEUR_ROUGE) && (grille[i][j] == ".") ){
			grille[i][j] = "O";
			tour++;
			return true;
		}
		else if( (getJoueurCourant() == JOUEUR_VERT) && (grille[i][j] == ".") ){
			grille[i][j] = "X";
			tour++;
			return true;
		}
	}
    return false;
}
